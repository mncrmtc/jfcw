jQuery(function ($) {
//Ease topbar links
	$('ul.navbar-nav li.leaf a').click(function(){
	    $('html, body').animate({
	        scrollTop: $( $.attr(this, 'href') ).offset().top
	    }, 500);
	    return false;
	});


//Projects!
//Les agrega clases & id y reubica el contenido en el media wrapper
	$('body.front .view-trabajos li.views-row').each(function (i) {
	    $(this).attr('id','proj-' + i);
	    $(this).find('a').attr('href','#media-' + i).addClass('media-tab');
	    $(this).find('.views-field-field-media').attr('id','media-' + i).appendTo('#media_wrapper');
	    $(this).find('.views-field-body').attr('id','body-content' + i).appendTo('#media_wrapper .views-field-field-media#media-' + i);
	});


$('#block-views-trabajos-block div.section-wrapper h2.block-title').after('<div class="title-separator" />');

//ejecuta el widget de tabs
$('body.front .view-trabajos').tabs({
    collapsible : true,
    active : false
});

//mete el titulo del proyecto dentro del anchor del logo
$('.view-trabajos ol.ui-tabs-nav li').each(function(){
	var linkLogo = $(this).find('.views-field-field-client-logo .field-content a');
	$(this).find('div:first').addClass('project-title').prependTo(linkLogo);
});

//por default el campo de contenido esta escondido, aunque esto deberia jalar normal desde el css :/
//$("body.front .view-trabajos #media_wrapper .views-field-field-media").css('display','none');

// //Projects effects
var headerHeight = $("body #navbar").height();
//anima el scroll al hacer click en el link del logo
	$("body.front .view-trabajos ol li").each(function(){ 
		$(this).find("a.media-tab").click(function(event){ 
			$('html, body').animate({
		        scrollTop: $( $.attr(this, 'href') ).offset().top - 120
		    }, 500);
		    return false;
		}); 
	});
 // El boton para cerrar 
	// $("body.front .view-trabajos div#close_btn").click(function(event){
	// 	$("body.front .view-trabajos .views-field-field-media.mediaDown").removeClass("mediaDown").animate({
	// 		height:'toggle'
	// 	},200);
	// 	return false;
	// });
	// $("body.front .view-trabajos div#prev_btn").click(function(event){
	// 	$("body.front .view-trabajos .views-field-field-media .field-content ul li").animate({
	// 		left:'+=600px'
	// 	},250);
	// 	return false;
	// });
	// $("body.front .view-trabajos div#next_btn").click(function(event){
	// 	$("body.front .view-trabajos .views-field-field-media .field-content ul li").animate({
	// 		left:'-=600px'
	// 	},250);
	// 	return false;
	// });

//Slide 
//Stablishes the slider inline width and height.
	// $("div.view-hero-unit div.view-content").attr('id','heroSlider').css({'height':'800','width':'100%'});
var heroHeight = $("div.view-hero-unit").height()

	$("div.view-hero-unit div.view-content").attr('id','heroSlider').css({'height':heroHeight,'width':'100%'});

	//The script requires the div has the ls-bg class otherwise it sends it to limbo.
	$("div.view-hero-unit div.view-content .ls-layer img").addClass('ls-bg');

	$("div.view-hero-unit div.view-content#heroSlider").layerSlider({
		skin : 'fullwidth',
		skinsPath : 'sites/all/themes/jfc/layerslider/skins/'
	});

// 	$("#heroSlider").addClass('cycle-slideshow').attr('data-cycle-slides' , '> div').attr('data-cycle-fx', 'scrollHorz').prepend('<div class="cycle-prev"></div><div class="cycle-next"></div>');


// Slide intento ejecutar el slider del hero unit dentro del tab...

	$("body.front .view-trabajos #media_wrapper .views-field-field-media ul").not('ul.contextual-links').addClass('proj-slider');

	$('body.front div.view-trabajos div.view-footer div#media_wrapper .views-field-field-media ul').addClass('cycle-slideshow').attr('data-cycle-slides', 'li').attr('data-cycle-fx', 'scrollHorz').attr('data-cycle-timeout', '0').prepend('<div class="cycle-prev"></div><div class="cycle-next"></div>');

//fix colorbox
	$("a.media-colorbox").colorbox({
		onComplete:function() {
		var x = jQuery('#cboxLoadedContent .file-image .content img').width();
		var y = jQuery('#cboxLoadedContent .file-image .content img').height();
		jQuery(this).colorbox.resize({width:x, height:y});
		},
	});


	$('body.not-front #navbar div.container  ul.navbar-nav li').each(function(){
			var menuLink = $(this).find('a').attr('href');
		        $(this).find('a').not('.soc-net').attr('href','home' + menuLink);
		        console.log (menuLink);

	});


	$('body.not-front #navbar div.container .tinynav-wrapper select.tinynav1 option').slice(0 , -2).each(function(){
			var optionLink = $(this).attr('value');
		        $(this).attr('value','home' + optionLink);

	});


	
}); 


