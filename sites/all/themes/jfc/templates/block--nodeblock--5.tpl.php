<section id="<?php print $block_html_id; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
	<div class="section-wrapper">
	  <?php print render($title_prefix); ?>
	  <?php if ($title): ?>
	    <h2<?php print $title_attributes; ?>><?php print $title; ?></h2>
	    <div class="title-separator"></div>
	  <?php endif;?>
	  <?php print render($title_suffix); ?>

	  <?php print $content ?>
		<div class="flexible-container">
	  		<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/ms?msa=0&amp;msid=215683213673948276014.0004e890cebd7dcd3cc2d&amp;hl=en&amp;ie=UTF8&amp;t=m&amp;ll=19.387996,-99.151912&amp;spn=0.014169,0.018239&amp;z=15&amp;output=embed"></iframe>
	  	</div>
	  	<span style="text-align:center; margin:5px; display:block;" class="copyright">© Copyright JF S.A. de C.V. México 2013. Todos los Derechos Reservados</span>
	</div>
</section> <!-- /.block -->
