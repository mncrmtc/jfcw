<section id="<?php print $block_html_id; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
	<div class="section-wrapper">
	  <?php print render($title_prefix); ?>
	  <?php if ($title): ?>
	    <h2<?php print $title_attributes; ?>><?php print $title; ?></h2>
	    <div class="title-separator"></div>
	  <?php endif;?>
	  <?php print render($title_suffix); ?>

	  <?php print $content ?>
	</div>
</section> <!-- /.block -->
